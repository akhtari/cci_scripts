"""Includes common methods for mining data."""

import requests


def get(url, params={}):
    """Sends a GET request to the given url with the given params

    Calls
    ----------
    <requests.get>

    Raises
    ----------
    HTTPError \n
    ConnectionError \n
    Timeout \n
    RequestException \n
        To handle other types of errors.

    Returns
    ----------
    response: requests.Response
    """

    try:
        response = requests.get(url=url, params=params)

    except requests.exceptions.HTTPError as errh:
        print("Http Error:", errh)

    except requests.exceptions.ConnectionError as errc:
        print("Error Connecting:", errc)

    except requests.exceptions.Timeout as errt:
        print("Timeout Error:", errt)

    except requests.exceptions.RequestException as err:
        print("OOps: Something Else", err)

    else:
        return response
