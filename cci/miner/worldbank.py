"""Handles fetching worldbank data from worldbank API (v2)."""

from collections import defaultdict
from json.decoder import JSONDecodeError
import statistics as stats
import math

import country_converter as coco
from colorama import init, Fore, Style

from cci.miner.common import get

init()


def _convert_country_keys_back_to_isonumeric(data):
    """Convert country keys back to iso numeric to update DB w/ them.
    While API uses iso3 country codes, DB uses iso numeric codes.
    Thus, convertion and re-convertion is necessary during communication.

    Parameters
    ----------
    data : Dict[str, float | "NULL"]
        key: countryiso3code
        value: Avg data of past five years of country, if unavailable, "NULL"

    Returns
    ----------
    Dict[int, float | "NULL"]
        key: country iso numeric code
    """

    def get_country_isonumeric(iso3):
        return coco.convert(iso3, to='ISOnumeric', not_found=None)

    countries_back_to_isonumeric = {get_country_isonumeric(country): value
                                    for country, value in data.items()
                                    if get_country_isonumeric(country) != None
                                    and isinstance(get_country_isonumeric(country), int)
                                    and get_country_isonumeric(country) == get_country_isonumeric(country)}

    return countries_back_to_isonumeric


def _get_last_five_years_avg_data(data):
    """Get last five years data for each country, then return its avg,
    if unavailable, return "NULL".

    Parameters
    ----------
    data : Dict[str, List[dict]]
        Dictionary of data grouped by country, each value includes list of dicts,
        including datapoints of each year (w/ value and date keys).
        (see: _get_grouped_by_country_data function).

    Calls
    ----------
    _get_avg

    Returns
    -------
    Dict[str, float | "NULL"]
        key: countryiso3code
        value: avg of past five years, if unavailable, "NULL".

    """

    last_five_years_data = {country: values[:5]
                            for country, values
                            in data.items()}

    def get_values_avg(values):
        try:
            values_list = [datum['value']
                           for datum in values if datum['value'] != None]
            avg = stats.mean(values_list)

        except (stats.StatisticsError, TypeError):
            return "NULL"

        return avg

    last_five_years_avg = {
        country: get_values_avg(values)
        for country, values in last_five_years_data.items()
    }

    return last_five_years_avg


def _get_grouped_by_country_data(data):
    """Group data fetched from API by countryiso3code key

    Parameters
    ----------
    data : dict
        Data fetched from API, w/o unnecessary keys and countries.
        (see: _filter_worldbank_data_by_country and _get_required_keys_from_worldbank_data functions)

    Returns
    -------
    Dict[str, List]
       {
           str (countryiso3code): [
               {
                   value: float
                   date: str
               }
           ]
       }
    """

    data_grouped_by_country = defaultdict(list)

    for datum in data:
        data_grouped_by_country[datum['countryiso3code']].append(
            {"value": datum["value"], "date": datum["date"]})

    return data_grouped_by_country


def _filter_worldbank_data_by_country(data, iso_numbers):
    """Get countries from the data fetched from API that exists in the db.

    Parameters
    ----------
    data : List[dict]
        Data fetched from API (only with necessary keys,
        see _get_required_keys_from_worldbank_data function).
    iso_numbers : list
        list of available ISO numbers, fetched from the API.

    Returns
    -------
    List[dict]
        Data filtered out by available countries in the db.
        {
            countryiso3code: str
            value: float
            date: str
        }
    """

    iso3codes = coco.convert(names=iso_numbers, to='ISO3', not_found=None)

    available_iso3codes = [code for code in iso3codes if code != None]

    filtered_data = [
        datum for datum in data if datum["countryiso3code"] in available_iso3codes]

    return data


def _get_required_keys_from_worldbank_data(data):
    """Filter out unecessary data from dict fetched from API.

    Parameters
    ----------
    data : List[dict] 
        Raw data fetched from API. 
        {
        indicator: {
            id: str
            value: str
            }
        country: {
            id: str
            value: str
            }
        countryiso3code: str
        date: str
        value: float | None
        unit: str
        obs_status: str
        decimal: int
        }

    Returns
    -------
    List[dict]
        Data with necessary keys.
        {
            countryiso3code: str
            value: float
            date: str
        }
    """

    filtered_keys = [{"countryiso3code": datum["countryiso3code"],
                      "value": datum["value"], "date": datum["date"]} for datum in data]

    return filtered_keys


def _fetch_worldbank_data_by_indicator(indicator):
    """Fetch data from Worldbank API for a single indicator.

    Parameters
    ----------
    indicator : str
        Indicator code of data to be fetched.

    Returns
    -------
    None | List[dict]
        Raw data, fetched from the Worldbank API.
    """

    url = f"http://api.worldbank.org/v2/country/all/indicator/{indicator}"

    # api returns 16104 data points per indicator
    params = {"format": "json", "per_page": "16104"}
    response = get(url, params)

    try:
        data = response.json()
    except JSONDecodeError:
        print(f'Decoding JSON response has failed for: {indicator}')
        return

    try:
        # json response is a list w/ 2 elements
        # the first one, a dict, includes request parameters and response errors
        # the second one, a list, includes time series data that we're looking for
        data = data[1]
    except IndexError:
        message = data[0]["message"][0]["value"]
        print(
            f"{Fore.RED} Something went wrong. Indicator: {indicator} - Error message: {message}")
        return

    return data


def get_worldbank_data_by_indicator(indicator, indicator_index, indicators_length, iso_numbers):
    """Fetchs and processes worldbank data for a single indicator

    Parameters
    ----------
    indicator : str
        Indicator code of data to be fetched. \n
    indicator_index : int
        Index of the current indicator in the indicators list, retrieved from the DB. \n
    indicators_length : int
        Length of the indicators list, retrieved from the DB. \n
    iso_numbers : list[int]
        List of ISO numeric code of available countries, retrieved from the DB. \n

    Calls
    -------
    1. _get_required_keys_from_worldbank_data: to filter unnecessary keys from list of dicts
        fetched from API. \n
    2. _filter_worldbank_data_by_country: to filter unnecessary countries from list of dicts
        fetched from API. \n
    3. _get_grouped_by_country_data: to group list of dicts by country. \n
    4. _get_last_five_years_avg_data: to take avg of last 5 years data for each country. \n
    5. _convert_country_keys_back_to_isonumeric: to convert keys of data dict from iso3 to ison. \n


    Returns
    -------
    Dict[str, float | "NULL"] | None
        Dictionary of fetched and processed data.
        key: country ISO numeric code
        value: avg of past five years, if unavailable, "NULL".
    """

    indicator_index_from_1 = indicator_index + 1

    print(f"{Fore.BLUE} Getting the data for: {indicator} ({indicator_index_from_1} of {indicators_length})...")

    data = _fetch_worldbank_data_by_indicator(indicator)

    if(data == None):
        print(
            f"{Fore.RED} Fetching data failed for: {indicator}. Received no data from the API.")
        return

    print(f"{Fore.GREEN} Successfully fetched data for: {indicator}. Cleaning the data now...")

    filtered_keys_data = _get_required_keys_from_worldbank_data(data)

    filtered_countries_data = _filter_worldbank_data_by_country(
        filtered_keys_data, iso_numbers)

    grouped_by_country_data = _get_grouped_by_country_data(
        filtered_countries_data)

    last_five_years_avg_data = _get_last_five_years_avg_data(
        grouped_by_country_data)

    data_with_isonumeric_countries = _convert_country_keys_back_to_isonumeric(
        last_five_years_avg_data)

    print(f"{Fore.GREEN} Successfully wrangled and cleaned the data for: {indicator}!")

    return data_with_isonumeric_countries


def _analyze_results(results):
    """Prints what fraction of the indicators failed to fetch/clean, along w/ specific failed codes.

    Parameters
    ----------
    results : 
        Dict[str, Dict[int, float | "NULL"] | None]
        key: code of data fetched from the API.
        value: data fetched from the API, processed.

    Returns
    ----------
    None
    """

    # If no data is available, we failed to fetch the indicator data.
    failed_codes = [code for code, data
                    in results.items() if data == None]

    print(f"{Fore.GREEN} Results: \n {len(failed_codes)} of {len(results)} failed. Failed to get data for:")

    for code in failed_codes:
        print(code)


def _get_non_null_results(results):
    """Get key, value pairs from dict of results where key and value are not null.

    Parameters
    ----------
    results : 
        Dict[str, Dict[int, float | "NULL"] | None]
        key: code of data fetched from the API.
        value: data fetched from the API, processed.

    Returns
    -------
    Dict[str, Dict[int, float | "NULL"]]
        Results w/ None values removed.
    """

    non_null_results = {key: value
                        for key, value in results.items()
                        if (value != None and key != None)}
    return non_null_results


def get_worldbank_data(indicators_with_codes, iso_numbers):
    """Get processed Worldbank data for the given list of indicators and given list of countries.

    Parameters
    ----------
    indicators_with_codes : List[Dict[str, str]]
        List of dictionaries w/ 2 keys, indicator and code. Retrieved from DB.
    iso_numbers : List[int]
        List of ISO numeric code of countries to fetch data for. Retrieved from DB.

    Calls
    ----------
    1. <get_worldbank_data_by_indicator> in a loop (dict comprehension) to fetch and process
    data for each indicator.

    2. <_analyze_results> to print a short overview of the failed indicators. 

    3. <_get_non_null_results> to remove None values from the results dict.

    Returns
    -------
    Dict[str, Dict[int, float | "NULL"]]
        key: code of the fetched data.
        value: dictionary of fetched data:
            key: country ISO numeric code
            value: avg of last five years of data, if unavailable, "NULL"
    """

    print(f"{Fore.BLUE} Started the mining process...")

    results = {item["code"]:
               get_worldbank_data_by_indicator(
        item["indicator"], index, len(indicators_with_codes), iso_numbers)
        for index, item in enumerate(indicators_with_codes)}

    print(f"{Fore.GREEN} Successfully fetched the data!")

    _analyze_results(results)

    print(Style.RESET_ALL)

    non_null_results = _get_non_null_results(results)

    if(not non_null_results):
        return

    return non_null_results
