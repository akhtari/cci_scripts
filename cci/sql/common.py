from collections import Counter
from urllib.parse import urlparse
import time
import datetime
import os


import mysql.connector
from mysql.connector import errorcode
from dotenv import load_dotenv
load_dotenv()

import cci.sql.queries.queries as queries

def connect():
    """Makes a connection to MySQL DB

    Returns
    -------
    connection, cursor
        connection: Union[CMySQLConnection, MySQLConnection]
        cursor: CMySQLCursor
    """
    try:
        USERNAME = os.getenv("DB_USERNAME")
        PASSWORD = os.getenv("DB_PASSWORD")
        HOST = os.getenv("DB_HOST")
        PORT = os.getenv("DB_PORT")
        DB = os.getenv("DB_NAME")

        connection = mysql.connector.connect(user=USERNAME,
                                             password=PASSWORD,
                                             host=HOST,
                                             port=PORT,
                                             database=DB)

    except mysql.connector.Error as error:

        if error.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your username or password.")

        elif error.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist.")

        else:
            print(error)

    else:
        cursor = connection.cursor()
        return connection, cursor


def _convert_list_of_tuples_to_list_of_strings(input):
    """Converts list of single-value tuples to a list of strings

    Parameters
    ----------
    input : List[Tuple(str, None)]
        List of single-value tuples, selected from DB.

    Returns
    -------
    List[str]
    """
    output = [x[0] for x in input]
    return output


def _get_list_of_dict(keys, list_of_tuples):
    """Given list of keys and tuples, returns a list of dicts

    Parameters
    ----------
    keys : List[str]
        List of keys for corresponding index in tuple.
    list_of_tuples : List[Tuple]
        Tuples to be turned into values of dict.

    Returns
    -------
    List[Dict]
        List of dicts, keys are taken from keys input, values from list_of_tuples.
    """
    list_of_dict = [dict(zip(keys, values)) for values in list_of_tuples]
    return list_of_dict


def _split_sources_string_by_semi_colon(string):
    """Splits source by semi colon, strips whitespace, and returns cleaned data.

    Note
    ----------
    Some of data in source column on summary table are incosistent. Where URL of source
    and name of it are entered together. We care only about URL, so we split selected
    data and take URL out. URL and source are sepatayed by a semicolon.


    Parameters
    ----------
    string : str
       Defective source, selected from DB.

    Returns
    -------
    str
        Cleaned data
    """
    return string.split(";")[0].strip()


def get_sources():
    """Selects all sources from summary table.

    Calls
    -------
    <connect> to make a connection to DB.
    <cursor.execute> to execute query to select sources.
    <cursor.fetchall> to get data from DB.
    <_convert_list_of_tuples_to_list_of_strings> to convert data.
    <_split_sources_string_by_semi_colon> to clean inconsistent data.

    Returns
    -------
    List[str]
        All sources in DB.
    """
    connection, cursor = connect()
    cursor.execute(queries.GET_SOURCE_FROM_SUMMARY_QUERY)

    data_list_of_tuples = cursor.fetchall()

    all_sources_list_of_strings = _convert_list_of_tuples_to_list_of_strings(
        data_list_of_tuples)

    all_sources = list(map(_split_sources_string_by_semi_colon,
                           all_sources_list_of_strings))

    return all_sources


def get_unique_sources_with_count():
    """Gets a list of dicts of all sources in summary table w/ their count.

    Calls
    -------
    1. <get_sources> to get a list of all sources from DB.
    2. <Counter> to count unique sources.


    Returns
    -------
    List[{"source": str, "count": int}]
        List of dicts including each source in summary table and their count.
    """

    all_sources = get_sources()

    sources_count = Counter(all_sources)

    # Get unique values by converting list to set
    unique_sources = list(set(all_sources))

    # Convert to a list of dicts w/ source and count values
    unique_sources_with_count = [{"source": source, "count": sources_count[source]}
                                 for source in unique_sources]

    # Sort list by count value
    unique_sources_with_count.sort(key=lambda x: x["count"], reverse=True)

    return unique_sources_with_count


def get_urls_with_codes():
    """Gets URL and code columns from summary table.

    Calls
    -------
    1. <connect> to make a connection to DB.
    2. <cursor.execute> to execute query to select URL with codes.
    3. <cursor.fetchall> to get data from DB.
    4. <_get_list_of_dict> to convert data.


    Returns
    -------
    List[{"code": str, "url": str}]
        URLs with their corresponding codes, selected from DB.
    """
    connection, cursor = connect()
    cursor.execute(queries.GET_URL_WITH_CODE_FROM_SUMMARY_QUERY)

    data_list_of_tuples = cursor.fetchall()

    all_urls_with_codes = _get_list_of_dict(
        ("code", "url"), data_list_of_tuples)

    return all_urls_with_codes


def _check_if_url_includes_netloc(url, netloc):
    """Given a URL and a netloc, checks if URL's netloc is same as given one 

    Parameters
    ----------
    url : str
        URL to be checked.
    netloc : str
        Netloc to be checked against.

    Returns
    -------
    bool
        Does URL include netloc?
    """
    url_netloc = urlparse(url).netloc
    return (url_netloc == netloc)


def get_urls_with_codes_by_netloc(netloc):
    """Given a netloc, gets code and URL column from summary table where URL includes netloc

    Parameters
    ----------
    netloc : str
        Netloc against which URLs are checked.


    Calls
    ----------
    1. <get_urls_with_codes> to get all URL and their corresponding codes from summary table.
    2. <_check_if_url_includes_netloc> to filter out URLs that don't include the given netloc.

    Returns
    -------
    List[{"url": str, "code": str}]
        list of dicts. Dicts are URLs w/ their corresponding codes selected from summary table.
    """
    all_urls_with_codes = get_urls_with_codes()

    filtered_urls_with_codes = list(filter(lambda item: _check_if_url_includes_netloc(item["url"], netloc),
                                           all_urls_with_codes))

    return filtered_urls_with_codes


def get_country_iso_numbers():
    """Gets all ISO numeric codes available in isomap table.

    Calls
    -------
    1. <connect> to make a connection to the DB.
    2. <cursor.execute> to execute query to select ISO numbers.
    3. <cursor.fetchall> to get data from the DB.


    Returns
    -------
    List[str]
        List of all ISO numeric codes available in isomap table.
    """
    connection, cursor = connect()
    cursor.execute(queries.GET_ISO_NUMBER_FROM_ISOMAP_QUERY)

    data_list_of_tuples = cursor.fetchall()

    iso_numbers = _convert_list_of_tuples_to_list_of_strings(
        data_list_of_tuples)
    return iso_numbers


def _generate_sql_script_filename(action):
    """Generates a unique name for SQL script file to be created.

    Parameters
    ----------
    action : str
        Action type of the script.

    Returns
    -------
    str
        Name of the file to be created.
    """
    time_in_millis = int(round(time.time() * 1000))

    now = datetime.datetime.now()
    date = f"{now.year}-{now.month}-{now.day}"

    filename = f"{action}-{time_in_millis}-{date}.sql"
    return filename


def create_sql_script_file(script, action):
    """Given a script and an action type, reates a .sql file w/ the script in ./scripts folder.

    Parameters
    ----------
    script : str
        refer to `cci.sql.worldbank._generate_sql_script_for_worldbank_data` return value.
    action : str
        Action type of the script.


    Calls
    ----------
    1. `_generate_sql_script_filename`
    2. `os.path.dirname`
    3. `os.path.realpath`
    4. `open`
    5. `file.write`
    6. `file.close`

    Returns
    -------
    TextIOWrapper
        IOWrapper of the file.
    """

    filename = _generate_sql_script_filename(action)

    dir_path = os.path.dirname(os.path.realpath(__file__))

    file = open(f"{dir_path}/scripts/{filename}", "x")
    file.write(script)
    file.close()

    return file


def execute_sql_scripts_from_file(filename):
    """Given the name of a SQL script file in ./scripts, execute the file in SQL server.

    Parameters
    ----------
    filename : str
        name of the file to be executed. Must be in ./scripts folder.

    Calls
    ----------
    1. `open`
    4. `connect`
    """
    file = open(f"{filename}", 'r')
    sql_file = file.read()
    file.close()

    # all SQL commands (split on ';')
    sql_commands = sql_file.split(';')

    connection, cursor = connect()

    # Execute every command from input file
    for command in sql_commands:
        try:
            result = cursor.execute(command)
        except mysql.connector.Error as error:
            print(f"error: {error.msg}")

    # Commit changes to db
    connection.commit()
