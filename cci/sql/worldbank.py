from cci.sql.common import get_urls_with_codes_by_netloc, create_sql_script_file
from cci.sql.actions import actions


def _get_worldbank_indicator_from_url(url):
    """Converts Worldbank API v1 URLS to Worldbank indicators.

    Note
    ----------
    Each Worldbank URL includes a unique indicator. While the URL structure of the new API
    has changed, the indicators are the same. We extract indicators to fetch their data from
    the new API.

    Parameters
    ----------
    url : string
        Worldbank API v1 URL

    Returns
    -------
    string
        Indicator in the URL.

    """

    indicator = url.split(
        "/")[-1]
    return indicator


def get_worldbank_indicators_with_codes():
    """Gets Worldbank URLs and codes from the DB, returns indicators and codes.

    Calls
    -------
    1. <cci.sql.common.get_urls_with_codes_by_netloc> (to select all rows w/ URL
    netloc of data.worldbank.org)

    2. <_get_worldbank_indicator_from_url> to convert Worldbank URLs to indicators.
    (note that the URLs in the DB are obsolete since Worldbank API v1 doesn't exist anymore.
    Indicators, however, are the same.) See _get_worldbank_indicator_from_url for more information.

    Returns
    -------
    List[{indicator: str, code: str}]
        A list of all Worldbank indicators and their corresponding codes in the summary table in the DB.
    """
    worldbank_urls_with_codes = get_urls_with_codes_by_netloc(
        "data.worldbank.org")

    worldbank_indicators_with_codes = [{"indicator": _get_worldbank_indicator_from_url(item["url"]),
                                        "code": item["code"]}
                                       for item in worldbank_urls_with_codes]

    return worldbank_indicators_with_codes


def _generate_sql_script_for_worldbank_data(data):
    """Given Worldbank data, generates a string containing the SQL script to update the DB.

    Parameters
    ----------
    data : data
    Dict[str, Dict[int, float | "NULL"]]
        key: code of the fetched data
        value: dictionary of fetched data:
            key: country ISO numeric code
            value: avg of last five years of data, if unavailable, "NULL"

    Returns
    -------
    str
        SQL script to update data table in the DB.
    """

    sql_script = ""

    try:

        for code in data.keys():

            try:

                values = data[code]

                for country_code in values.keys():

                    try:

                        value = values[country_code]
                        sql_script = sql_script + \
                            f"UPDATE data SET `{code}` = {value} WHERE IsoN = {country_code};"

                    except KeyError:
                        print(
                            f"No data for code{code} and country code {country_code}.")

            except (KeyError, AttributeError):
                print(f"No data for code {code}.")

    except AttributeError:
        print("Data is not available or in the correct format.")

    return sql_script


def create_update_worldbank_sql_script_file(data):
    """Takes Worldbank data and creates a SQL script file to update the DB based on the data. 

    Parameters
    ----------
    data : data
    Dict[str, Dict[int, float | "NULL"]]
        key: code of the fetched data
        value: dictionary of fetched data:
            key: country ISO numeric code
            value: avg of last five years of data, if unavailable, "NULL"

    Calls
    ----------
    1.<_generate_sql_script_for_worldbank_data> to generate SQL script string.

    2.<cci.sql.common.create_sql_script_file> to create the script file and
    write script string in it.

    Returns
    -------
    TextIOWrapper
        IOWrapper of the newly created .sql file.
    """

    sql_script = _generate_sql_script_for_worldbank_data(data)

    file = create_sql_script_file(
        sql_script, actions.ACTION_TYPES["UPDATE_WORLDBANK_DATA_ACTION"])

    return file
