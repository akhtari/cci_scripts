import cci.miner.worldbank as worldbank_miner
import cci.sql.worldbank as worldbank_sql
import cci.sql.common as common_sql


def update_worldbank_data():
    """Controls the procedure of retrieving and updating worldbank data.

    Steps: \n
        1. Get worldbank indicators with their corresponding codes from the db.
        2. Get ISO numeric code of available countries from the db.
        3. Fetch worldbank data for the given indicators and countries.
        4. Raise TypeError if data is NoneType.
        5. Create a sql script file to update the data table in the db.
        6. Execute the sql script file.

    Calls
    ----------
    get_worldbank_indicators_with_codes from cci.sql.worldbank \n
    get_country_iso_numbers from cci.sql.common \n
    get_worldbank_data from cci.miner.worldbank \n
    create_update_worldbank_sql_script_file from cci.sql.worldbank \n
    execute_sql_scripts_from_file from cci.sql.common \n

    Raises
    ----------
    TypeError
        If fetched data from the API is NoneType.

    Returns
    ----------
    None
    """

    indicators_with_codes = worldbank_sql.get_worldbank_indicators_with_codes()

    iso_numbers = common_sql.get_country_iso_numbers()

    print("Getting the latest data...")
    data = worldbank_miner.get_worldbank_data(
        indicators_with_codes, iso_numbers)

    if data == None:
        raise TypeError("Data fetched from the API is NoneType.")

    print("Creating the sql script file...")
    file = worldbank_sql.create_update_worldbank_sql_script_file(data)

    print("Running the sql script file...")
    common_sql.execute_sql_scripts_from_file(file.name)


    if __name__ == "__main__":
        update_worldbank_data()