[#] Update inconsistent source
[#] Error handling and feedback
[#] Create sql query
[#] Export data to SQL
[#] Run scripts w/ Ale
[#] Check necessary indicators
[#] Refactor design
[#] Update logic (last five years avg)
[#] Add types
[#] Add docstrings
[#] Push DB to sql server
[#] Add docstring for remaining functions
[#] Update wrong indicator names and columns
[] Get worldbank data
[] Get indicators for PLS SEM